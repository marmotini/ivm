## Details

<!--
Provide a detailed description of the bug you encountered, what you expected to
happen, etc. Please provide as many details as possible.
-->

## How to reproduce the bug

<!--
Include a list of steps that reproduce the bug. Please reduce this list of steps
to the essentials, as this makes it easier for maintainers to help you.
-->

## System information

<!-- Replace XXX with the value, such as the ivm version -->

* Operating system (Linux, macOS, etc): XXX
* ivm version (see `ivm --version`): XXX
* Rust version (see `rustc --version`): XXX
* cargo version (see `cargo --version`): XXX

/label ~"type::Bug"
